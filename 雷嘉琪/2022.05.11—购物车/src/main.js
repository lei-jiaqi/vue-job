import Vue from 'vue';
import App from './App.vue';

import VueRouter from 'vue-router';
import Vuex from 'vuex';

import router from './router/index';
Vue.config.productionTip = false;

// 应用插件
Vue.use(VueRouter);
Vue.use(Vuex);

// vuex 配置
const store = new Vuex.Store({
  state: {
    nz:[
      { name:'乐町女装专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/21/138/ias_8391d5e234109644d4c78cc0ca335210_1135x545_85.jpg', discount: '1.3', zi: '折起', price:2000, num: 1 },
      { name:'三彩女装专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/04/26/179/ias_7fccd79943a2e752a2ba78bb5e486fae_1135x545_85.jpg', discount: '4.8', zi: '折封顶', price:3000, num: 1 },
      { name:'百图女装疯抢专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/04/13/93/ias_1f731cf4dd52ef228801b424a1a424c8_1135x545_85.jpg', discount: '2.7', zi: '折封顶', price:4000, num: 1 },
  ],
    ch:[
      { name: '玉兰油OLAY护肤个护专场', img: 'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/04/12/163/ias_00742d70e69101b51dbfceeb08f27779_1135x545_85.jpg', discount: '3折', zi: '折起', price: 2000, num: 1 },
      { name: '薇诺娜winona面部护理专场', img: 'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/31/169/ias_0af7b2dd54e11a247042968678401c60_1135x545_85.jpg', discount: '2.3', zi: '折起', price: 100, num: 1 },
      { name: '芙丽芳丝freeplus美容护肤专场', img: 'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/01/04/177/ias_c22e5ffafec895484e7f5f718b5751fd_1135x545_85.jpg', discount: '6.3折', zi: '封顶', price: 200, num: 1 },
  ],
    ss:[
      { name:'施华洛世奇饰品专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/16/192/ias_da1002a855a85413b4fefe9935873593_1135x545_85.jpg', discount: '2', zi: '折起', price:80000, num: 1 },
      { name:'周生生珠宝首饰专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/17/196/ias_ccd08b7e5490346c2f41ec57bcd34af3_1135x545_85.jpg', discount: '6', zi: '折起', price:20000, num: 1 },
      { name:'卡西欧CASIO石英表专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/27/165/ias_643a82c2fc053026529445843edb094b_1135x545_85.jpg', discount: '2', zi: '折起', price:120000, num: 1 },
  ],
    tz:[
      { name:'巴帝巴帝Badibadi童装专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/03/02/0/ias_34e94827f742526868d0b303baaaa718_1135x545_85.jpg', discount: '5折', zi: '封顶', price:600, num: 1 },
      { name:'木木屋童装专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/04/14/36/ias_f18e0ae7e32e29097900f3059a468213_1135x545_85.jpg', discount: '4.3', zi: '折封顶', price:800, num: 1 },
      { name:'芭芭鸭童鞋专场', img:'https://h2.appsimg.com/a.appsimg.com/upload/brand/upcb/2022/04/18/72/ias_862665296a3d9907061edb921788b3cd_1135x545_85.jpg', discount: '3.8', zi: '折封顶', price:400, num: 1 },
  ]
}
})

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
