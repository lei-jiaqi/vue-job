import VueScroller from 'vue-scroller';

const plugin1 = {

    install: function (Vue) {

        Vue.use(VueScroller);

    }

}

export default plugin1;