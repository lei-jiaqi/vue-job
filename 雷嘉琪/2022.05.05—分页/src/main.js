import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router/index'
Vue.config.productionTip = false

import axios from 'axios'

// 拉取页面
import PlugIns from './plugins/PlugIns';
Vue.use(PlugIns)

Vue.prototype.$axios=axios;

Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router:router
}).$mount('#app')
