var express = require('express');
var conn = require('../mysql.js');
//建立一个路由容器
var router = express.Router();
router.get("/getbooks", (req, res) => {
    var sql = "select * from books";
    conn.query(sql, (err, result, fields) => {
        if (err) {
            return "失败";
        }
        console.log(result);
        res.send(result);
    })
});
router.get("/updatebook", (req, res) => {
    let id = req.query.id;
    let name = req.query.name;
    console.log("id=" + id);
    console.log("name=" + name);
    conn.query("update books set name=? where id=?", [name, id], (err, result) => {
        if (err) {
            return "失败";
        }
        console.log(result);
        res.send(result);
    })
})

router.get("/delbook", (req, res) => {
    let id = req.query.id;
    console.log("id=" + id);
    conn.query("delete from books where id=?", [id], (err, result) => {
        if (err) {
            return "失败";
        }
        console.log(result);
        res.send(result);
    })
})

router.get("/addbook", (req, res) => {
    let id = req.query.id;
    let name = req.query.name;
    console.log("id=" + id);
    conn.query("insert into books(id,name) values(?,?)", [id, name], (err, result) => {
        if (err) {
            return "失败";
        }
        console.log(result);
        res.send(result);
    })
})
//导出
module.exports = router;